# cmc-democenter-server

runs the parent server process of the api, product-center and admin sites

```
# run the server
npm run watch:server
```


## proxy

 * 9000: this proxy
 * 9001: cmc-site static site grunt connect server
 * 9002: browser sync for product center -> 9003
 * 9003: cmc-democenter-server nodemon app
 * 9004: browser sync for admin -> 9003
 

``` 
# run the proxy
npm run watch:proxy
```