// eslint-disable-next-line import/no-extraneous-dependencies
const dotEnv = require('dotenv');

if (process.env.NODE_ENV !== 'production') {
    dotEnv.config();
}
const express = require('express');
const path = require('path');
const parseDomain = require('parse-domain');
const exphbs = require('express-handlebars');
const subdomain = require('express-subdomain');
const helmet = require('helmet');
const favicon = require('serve-favicon');
const cmcApi = require('cmc-api/app');
const errorLogger = require('./lib/error-logger');

const app = express();
const { writeErrorToLog } = errorLogger(app);
process.on('uncaughtException', writeErrorToLog);

app.use(helmet());
app.use(helmet.hidePoweredBy({ setTo: 'PHP 4.2.0' }));
app.set('case sensitive routing', true);
app.set('views', path.join(__dirname, 'views'));
app.engine('.hbs', exphbs({ extname: '.hbs', defaultLayout: null }));
app.set('view engine', '.hbs');
app.use(favicon(`${__dirname}/public/favicon.ico`));

app.use(subdomain(
    parseDomain('https://admin-3.suhrthing.com').subdomain,
    (req, res, next) => {
        res.redirect(`https://admin-4.campusmanagement.com${req.originalUrl}`);
    },
));

app.use(subdomain(
    parseDomain(process.env.CORS_ORIGIN_API).subdomain,
    cmcApi,
));
app.use(express.static(path.join(__dirname, 'public')));

app.get('/', (req, res) => {
    res.json([
        { name: 'Steve' },
    ]);
});

// catch 404 and forward to error handler
app.use((req, res, next) => {
    const err = new Error('Not Found');
    err.status = 404;
    return next(err);
});

app.use((err, req, res, next) => {
    if (err) {
        writeErrorToLog(err, req);
    }
    if (err && err.name === 'UnauthorizedError') {
        return res.redirect('/login');
    }
    res.status(err.status || 500);
    const error = (app.get('env') !== 'production')
        ? err : {};
    return res.render('error', {
        message: err.message,
        error,
    });
});

module.exports = app;
