const express = require('express');
const fs = require('fs');
// eslint-disable-next-line import/no-extraneous-dependencies
const proxy = require('http-proxy-middleware');
const https = require('https');
const subdomain = require('express-subdomain');

const app = express();

/**
 * 9000: this proxy
 * 9002: cmc-site static site grunt connect server
 * 9003: cmc-democenter-server nodemon app
 * 9004: browser sync for admin -> 9003
 * 9006: weinre
 */

const sslOptions = {
    key: fs.readFileSync('/etc/letsencrypt/live/suhrthing.com/privkey.pem', 'utf8'),
    cert: fs.readFileSync('/etc/letsencrypt/live/suhrthing.com/cert.pem', 'utf8'),
    ca: fs.readFileSync('/etc/letsencrypt/live/suhrthing.com/chain.pem', 'utf8'),
    requestCert: false,
    rejectUnauthorized: false,
};

app.use(subdomain('api', proxy({
    target: 'https://api.suhrthing.com:9003',
    changeOrigin: false,
    ws: true,
    ssl: sslOptions,
    xfwd: true,
    secure: false,
})));

app.use(subdomain('admin', proxy({
    target: 'https://localhost:9004',
    changeOrigin: false,
    ws: true,
    ssl: sslOptions,
    secure: false,
    xfwd: true,
    pathRewrite: (path, req) => {
        if (/^\/(.*[\\\/])(.+?)(?=hot-update\.js)/.test(path)) {
            // eslint-disable-next-line no-console
            console.log('in', path,
                'out', path.replace(/^\/(.*[\\\/])(.*)(.hot-update.js)$/, '/$2$3')
            );
        }
        return path.replace(/^\/(.*[\\\/])(.+?)(?=hot-update\.js)/, '/$2');
    },
})));

const cmcRouter = new express.Router();

cmcRouter.use('/weinre', proxy({
    target: 'http://localhost:9006',
    changeOrigin: false,
    ws: true,
    // ssl: sslOptions,
    secure: false,
    pathRewrite: { '^/weinre': '' },
}));

cmcRouter.use('/ws', proxy({
    target: 'http://localhost:9006',
    changeOrigin: false,
    ws: true,
    // ssl: sslOptions,
    secure: false,
    // pathRewrite: { '^/weinre': '' },
}));

cmcRouter.use(proxy({
    target: 'https://localhost:9002',
    changeOrigin: false,
    ws: true,
    ssl: sslOptions,
    secure: false,
}));

app.use(subdomain('cmc', cmcRouter));

const server = https.createServer(sslOptions, app);
server.listen(9000);
