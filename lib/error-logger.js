const fs = require('fs');
const path = require('path');
const morgan = require('morgan');
const FileStreamRotator = require('file-stream-rotator');

module.exports = function errorLogger(app) {
    let errorLogStream = false;
    if (process.env.NODE_ENV !== 'production') {
        const logDirectory = path.join(process.cwd(), 'log');
        if (!fs.existsSync(logDirectory)) {
            fs.mkdirSync(logDirectory);
        }
        const accessLogStream = FileStreamRotator.getStream({
            date_format: 'YYYYMMDD',
            filename: path.join(logDirectory, 'access-%DATE%.log'),
            frequency: 'daily',
            verbose: false,
        });
        app.use(morgan('dev'));
        app.use(morgan('combined', {
            stream: accessLogStream,
            skip: (req, res) => req.method === 'OPTIONS',
        }));
        errorLogStream = FileStreamRotator.getStream({
            date_format: 'YYYYMMDD',
            filename: path.join(logDirectory, 'error-%DATE%.log'),
            frequency: 'daily',
            verbose: false,
        });
    }

    return {
        writeErrorToLog: function writeErrorToLog(error, req) {
            const date = new Date();
            const url = req ? `${req.originalUrl}\n` : '';
            const status = error.status ? `Status: ${error.status}\n` : '';
            const message = error.message ? `Message: ${error.message}\n` : '';
            const stack = error.stack && error.status && error.status !== 404 ? `${error.stack}\n` : '';
            const errString = `server-error-log: ${date}\n${url}${status}${message}${stack}\n`;

            if (!error.status || (error.status && error.status !== 404)) {
                console.error(errString); // eslint-disable-line no-console
            }
            if (errorLogStream) {
                errorLogStream.write(errString);
            }
        },
    };
};
